import { getTokenCookie } from '../utils/auth'

export default function ({ redirect, req }) {
//   console.log(context)
  const cookies = process.client ? document.cookie : req?.headers?.cookie
  if (!getTokenCookie(cookies)) {
    return redirect('/login')
  }
}
