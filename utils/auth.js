const tokenName = 'token'

export function login (token) {
  try {
    let date = new Date(Date.now() + 86400e3)
    date = date.toUTCString()
    document.cookie = `${tokenName}=${token}; expires=${date}`
    return true
  } catch (error) {
    return false
  }
}

export function logout () {
  document.cookie = `${tokenName}=; expires=-1`
  this.$router.push('/login')
}

export function getTokenCookie (container) {
  const tokenCookie = container.split('; ').find(el => el.includes(tokenName))
  if (tokenCookie) {
    return tokenCookie.split('=')[1]
  } else {
    return false
  }
}
