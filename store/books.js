export default {
  state: {
    books: [],
    books_count: 0
  },
  actions: {
    SEARCH_BOOKS ({ commit }, params) {
      return new Promise((resolve, reject) => {
        this.$axios.get('/volumes', { params }).then((res) => {
          commit('SET_BOOKS', res.data.items)
          commit('SET_BOOKS_COUNT', res.data.totalItems)
          resolve(res.data.items)
        }).catch((err) => {
          reject(err)
        })
      })
    }
  },
  mutations: {
    SET_BOOKS (state, data) {
      state.books = data
    },
    SET_BOOKS_COUNT (state, data) {
      state.books_count = data
    }
  },
  getters: {
    getBooks (state) {
      return state.books
    }
  }
}
